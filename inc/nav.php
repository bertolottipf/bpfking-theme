<!-- Main jumbotron for a primary marketing message or call to action -->

<nav id="nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation" <?php if (is_user_logged_in()) { echo('style="margin-top:32px"'); } ?>>
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img id="logobar" src="<?php echo get_template_directory_uri(); ?>/images/LogoBPF-little.png">
		</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">

		<?php wp_nav_menu( array(
			'theme_location' => 'primary',
			'menu' => '', 
			'container' =>false,
			'menu_class' => 'nav navbar-nav navbar-right bar',
			'echo' => true,
			'before' => '',
			'after' => '',
			'link_before' => '',
			'link_after' => '',
			'depth' => 2,
			'walker' => new Bootstrap_Nav_Walker()
			) ); ?>
		</div><!-- .navbar-collapse -->
	</nav>
