<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since BPFKing 2.0
 */

	// Options Framework (https://github.com/devinsays/options-framework-plugin)
if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
	require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}

	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
function bpfking_setup() {
	load_theme_textdomain( 'bpfking', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
	add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Navigation Menu', 'bpfking' ) );
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'bpfking_setup' );

	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
function bpfking_scripts_styles() {
	global $wp_styles;

		// Load Comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

		// Load Stylesheets
//		wp_enqueue_style( 'bpfking-reset', get_template_directory_uri() . '/reset.css' );
//		wp_enqueue_style( 'bpfking-style', get_stylesheet_uri() );

		// Load IE Stylesheet.
//		wp_enqueue_style( 'bpfking-ie', get_template_directory_uri() . '/css/ie.css', array( 'bpfking-style' ), '20130213' );
//		$wp_styles->add_data( 'bpfking-ie', 'conditional', 'lt IE 9' );

		// Modernizr
		// This is an un-minified, complete version of Modernizr. Before you move to production, you should generate a custom build that only has the detects you need.
		// wp_enqueue_script( 'bpfking-modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2.dev.js' );

}
add_action( 'wp_enqueue_scripts', 'bpfking_scripts_styles' );

	// WP Title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
function bpfking_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

//		 Add the site name.
	$title .= get_bloginfo( 'name' );

//		 Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

//		 Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'bpfking' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'bpfking_wp_title', 10, 2 );






//OLD STUFF BELOW


	// Load jQuery
if ( !function_exists( 'core_mods' ) ) {
	function core_mods() {
		if ( !is_admin() ) {
			wp_deregister_script( 'jquery' );
			wp_register_script( 'jquery', ( "http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ), false);
			wp_enqueue_script( 'jquery' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'core_mods' );
}

	// Clean up the <head>, if you so desire.
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');

	// Custom Menu
register_nav_menu( 'primary', __( 'Navigation Menu', 'bpfking' ) );

	// Widgets
if ( function_exists('register_sidebar' )) {
	function bpfking_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Sidebar Widgets', 'bpfking' ),
			'id'            => 'sidebar-primary',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
			) );
	}
	add_action( 'widgets_init', 'bpfking_widgets_init' );
}

	// Navigation - update coming from twentythirteen
function post_navigation() {
	echo '<div class="navigation">';
	echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
	echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
	echo '</div>';
}

	// Posted On
function posted_on() {
	printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_author() )
		);
}







/*
**
** Twitter Bootstrap default menu
**
*/

$parents = array();

// add a parent class for menu item 
add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );
function add_menu_parent_class( $items ) {
	global $parents;
	
	foreach ( $items as $item ) {
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}
	
	return $items;    
}


class Bootstrap_Nav_Walker extends Walker_Nav_Menu {

	function start_lvl(&$output, $depth = 0, $args = array()) {
		$output .= "\n<ul class=\"dropdown-menu\">\n";
	}

	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $parents;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		
		if ( in_array( $item->ID, $parents ) )
			$classes[] = 'dropdown';

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		if ( in_array( $item->ID, $parents ) ) {
			$atts['class'] = 'dropdown-toggle';
			$atts['data-toggle'] = 'dropdown';
		}
		
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		
		$item_output .= in_array( $item->ID, $parents ) ? ' <b class="caret"></b>' : '';
		
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

}


function add_my_scripts() {
	wp_enqueue_script(
		'functions', // name your script so that you can attach other scripts and de-register, etc.
		get_template_directory_uri() . '/js/functions.js', // this is the location of your script file
		array('jquery') // this array lists the scripts upon which your script depends
		);
	wp_enqueue_script(
		'main', // name your script so that you can attach other scripts and de-register, etc.
		get_template_directory_uri() . '/js/main.js', // this is the location of your script file
		array('jquery') // this array lists the scripts upon which your script depends
		);
}
add_action( 'wp_enqueue_scripts', 'add_my_scripts' );


?>
