<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since BPFKing 2.0
 * Template Name: page's articles
 */
 get_header(); ?>
<div id="content" role="main">
<div class="col-xs-8" role="main">
	<h2><?php the_title(); ?></h2>
	<?php
	$query= 'orderby=date&order=DESC';
	query_posts($query);
	?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">

			<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>

			<?php posted_on(); ?>

			<div class="entry">

				<?php
					//the_content();
					the_excerpt();
				?>

				<?php wp_link_pages(array('before' => __('Pages: ','bpfking'), 'next_or_number' => 'number')); ?>

			</div>

			<?php edit_post_link(__('Edit this entry','bpfking'), '<p>', '</p>'); ?>

		</article>
		
		<?php comments_template(); ?>

		<?php endwhile; endif; ?>
</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
</div>