#  BPFKing WordPress Theme

BPFKing is a  responsive WordPress theme based in HTML5, JQuery and Bootstrap with wood images by default

The homepage is home.php if it exists.

## Some of the features:

1. Use Jquery
2. Use HTML5
3. Use Bootstrap

## Suggested Plugin

Use __Nimble Portfolio__ to use better this site.
