<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since BPFKing 2.0
 */
get_header(); ?>
<?php 
if (true == of_get_option('layout')){
	switch ( of_get_option('layout') )
	{
		case '1col':
		$content_area_position = 'center';
		$sidebar_container_position = false;
		break;
		case '2cols-r':
		$content_area_position = 'left';
		$sidebar_container_position = 'right';
		break;
		case '2cols-l':
		$content_area_position = 'right';
		$sidebar_container_position = 'left';
		break;
		default:
		$content_area_position = 'right';
		$sidebar_container_position = 'left';
	}
}
?>


<div id="content" role="main">
<div  role="main">
	
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div id="caruselbg1" class="bgcaruselitem">&nbsp;</div>
				<div class="carousel-caption-bpf" id="carousel-caption-bpf-1">
					<h1><img src="<?php echo get_template_directory_uri(); ?>/images/LogoBPFblack.png" style="width:200px" alt="BPFKing"></h1>
					<h4><em>Riscopri il piacere dell’antichità in un pezzo di legno saggiamente lavorato a mano.</em></h4>
				</div>
			</div>
			<div class="item">
				<div id="caruselbg2" class="bgcaruselitem">&nbsp;</div>
				<div class="carousel-caption-bpf" id="carousel-caption-bpf-2">
					<h2>Obiettivo</h2>
					<h4><em>BPFKing dà spessore ai tuoi disegni e foto.</em></h4>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div> <!-- /carousel -->



	<div class="jumbotron" id="badge">
		
		<div class="row">
			<div class="col-sm-4 col-xs-12 equalrowcontainer">
				<div class="thumbnail equalrowcontent">
					<img src="<?php echo get_template_directory_uri(); ?>/images/contact.png">
					<div class="caption">
						<h3><strong>Contatto</strong></h3>
						<p class="text-justify">Contattaci per avere un’idea di come potrebbe risultare il lavoro, inviandoci anche il file di cui vuoi il lavoro</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12 equalrowcontainer">
				<div class="thumbnail equalrowcontent">
					<img src="<?php echo get_template_directory_uri(); ?>/images/reso.png">
					<div class="caption">
						<h3><strong>Messe a punto</strong></h3>
						<p class="text-justify">Vi risponderemo con un’idea del lavoro risultante aprendo una discussione per accordarci.</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12 equalrowcontainer">
				<div class="thumbnail equalrowcontent">
					<img src="<?php echo get_template_directory_uri(); ?>/images/time.png">
					<div class="caption">
						<h3><strong>Attesa</strong></h3>
						<p class="text-justify">Dovrete attendere l’arrivo del lavoro presso Voi. I nostri lavori sono al <strong>100% artigianali</strong>.</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /badges -->



	<!--<div class="clearfix"></div>
	<div class="col-md-12">
		< ?php echo do_shortcode('[nimble-portfolio-carousel ]'); ?>
	</div>-->




	<div class="white spaziatori">
		<div class="col-xs-12 col-md-6">
			<p><input type="text" name="nome" placeholder="Nome" size="40"></p>
			<p><input type="text" name="email" placeholder="Email" size="40"></p>
			<p><input type="text" name="oggetto" placeholder="Oggetto" size="40"></p>
			<p><textarea type="text" name="messaggio" placeholder="Messaggio" rows="10" cols="40"></textarea></p>
		</div>
		<div class="col-xs-12 col-md-6">
			<p>Contattateci senza remore, per avere informazioni aggiuntive, conoscere come lavoriamo o per avere un preventivo.</p>
			<p>Raccomandiamo di lasciare nel messaggio le vostre richieste dettagliate (materiale, solo soggetto/con sfondo completo, solo contorni/ombreggiato) lasciando una foto dell’opera che desiderate avere in modo indelebile.</p>
			<p>Raccomandiamo di lasciare per ogni evenienza anche un recapito telefonico.</p>
			<p>Vi contatteremo il più presto.</p>
		</div>
	</div>





</div>
<?php get_footer(); ?>
</div>
	


