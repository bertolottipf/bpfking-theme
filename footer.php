<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since BPFKing 2.0
 */
?>
			<div class="clearfix"></div>
			<div id="footer">
				<div class="row">
					<div class="col-xs-4"></div>
					<div class="col-xs-4"></div>
					<div class="col-xs-4"></div>
				</div>
				<div class="row">
					<div class="col-xs-6">Sito creato e gestito da BERTOLOTTI Paolo Francesco</div>
					<div class="col-xs-6"></div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- #wrapper -->

	<?php wp_footer(); ?>


<!-- jQuery is called via the WordPress-friendly way via functions.php -->
<script src="<?php bloginfo('template_directory'); ?>/js/vendor/bootstrap.min.js"></script>
<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/js/functions.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<?php if (is_home()) { ?>
	<script src="<?php bloginfo('template_directory'); ?>/js/homepage.js"></script>
<?php } ?>
<!-- Asynchronous google analytics; this is the official snippet.
         Replace UA-XXXXXX-XX with your site's ID and domainname.com with your domain, then uncomment to enable.

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-XXXXXX-XX', 'domainname.com');
  ga('send', 'pageview');

</script>
-->

</body>

</html>
