

function equalHeight(group) {
	tallest = 0;
	group.each(function() {
		thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	group.height(tallest);
}

function nimbleCarousel() {
	//cambio l'id del contenitore del mio carousel
	$('.nimble-portfolio').attr("id","nimble-carousel");
	// cambio le classi del div contenitore
	$('#nimble-carousel').addClass('carousel slide');
	$('#nimble-carousel').removeClass('nimble-portfolio   -skin-default -skin-default-normal -columns3');


	//rimuovo il div filtri
	$('.-filters ').remove();
	

	// cambio al div .-items la classe con .carousel-inner
	$('.-items').addClass('carousel-inner');
	$('#nimble-carousel .carousel-inner').removeClass('-items');
	// aggiungo al div .carousel-inner la proprietà role="listbox"
	$('#nimble-carousel .carousel-inner').attr("role", "listbox");


	//cambio al div .-item  la classe con .item
	$('.-item').addClass('item');
	$('#nimble-carousel .item').removeClass('-item');


	// aggiungo a .item un div contenente immagine con src uguale a qualla del div itembox
	/*console.log('<img src="'+
				$('.itembox  img').prop('src')
				+'">');*/
	$('#nimble-carousel .item').prepend(
		'<div class="col-md-4"><img src="'+
		$('.itembox  img').prop('src')
		+'">'
		);
	$('.itembox').remove();



	//aggiunto il carousel-caption
	$('.title').addClass('.carousel-caption');
	$('.carousel-caption').removeClass('.title');

	$('#nimble-carousel .item').append('</div>');


	// aggiungo i controllori
	$('#nimble-carousel').append('<!-- Controls -->\
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>\
		<span class="sr-only">Previous</span>\
		</a>\
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">\
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>\
		<span class="sr-only">Next</span>\
		</a>\
		</div>');

	//$('#nimble-carousel').carousel();

}
