<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since BPFKing 2.0
 */
get_header(); ?>
<div id="content" role="main">
	<div class="col-sm-8" role="main">
		<h2><?php _e('Error 404 - Page Not Found','bpfking'); ?></h2>
	</div>
	<?php get_sidebar(); ?>

	<?php get_footer(); ?>
</div>